# FasterWhisperGui

## WARNING

***This deposit is no longer maintained.***

You can use the cli version, with the minimalist gui build with zenity [here](https://gitlab.com/Breizhux/faster-whisper-cli).

---

FasterWhisperGui is a Gtk interface build in python for whisper speech-to-text engine.
This software does not use the official [OpenAI Whisper](https://github.com/openai/whisper) software, but the [faster-whisper](https://github.com/guillaumekln/faster-whisper) fork.
Inspired by [whisper-gui-app](https://github.com/iamironman0/whisper-gui-app)

If you want a real time speech-to-text engine, see my [vosk gui project](https://gitlab.com/Breizhux/voskboard)!

![Faster-Whisper-Gui running...](img/faster_whisper_gui_running.png)

## Getting started

To install this software on debian-based system, run this :

```bash
sudo apt install -y git virtualenv python3-gi #apt dependancy
git clone https://gitlab.com/Breizhux/faster-whisper-gui
cd faster-whisper-gui
chmod +x build.sh
./build.sh
sudo apt install ./faster-whisper-gui.deb
```

Then, you can remove the downloaded folder `faster-whisper-gui`.

## Run

To start software you can go to *menu* > *Accessory* > *FastWhisper*

Or command line : `faster-whisper-gui`

## Models

All [Whisper-like](https://github.com/openai/whisper#available-models-and-languages) models are available (Large, Medium, Small, Base, Tiny). They are just converted to correspondig CTranslate2 model. More informations [here](https://github.com/guillaumekln/faster-whisper#large-v2-model-on-gpu)

**Create an Issue to remind me to add your language in the Gtk interface !**

Thanks to Faster-Whisper, the Medium model only requires 1.5GB of Ram!

## Uninstall

On debian-like system, just run this command :
```bash
sudo apt remove --purge --auto-remove fasterwhispergui
```

