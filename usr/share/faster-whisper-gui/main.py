#!/usr/bin/env python3
#! -*- coding : utf-8 -*-

import os
from threading import Thread
from faster_whisper import WhisperModel

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class FastWhisperGui() :
    widgets_name = ["model_size", "english_only", "language", "input_file",
                    "generate_subtitle_file", "save_to_file", "use_gpu",
                    "precision"]
    models = [
        "tiny",
        "base",
        "small",
        "medium",
        "large-v2"
    ]
    languages = [
        "auto",
        "fr",
        "en",
        "du",
    ]
    precision = [
        "int8",
        "float16",
        "float32"
    ]
    def __init__(self) :
        #transcription
        self.text = [] #{start:* , end:* , text:*}
        self.__lock = False #if True, the current transcription will stop without end
        #interface
        self.interface = Gtk.Builder()
        self.interface.add_from_file("./gui.glade")
        #set content of interface
        #self.__set_available_lang()
        #show interface
        self.window = self.interface.get_object("window")
        self.window.connect("delete-event", self.cancel)
        self.window.show()
        #connect interface to self
        self.interface.connect_signals(self)

    def cancel(self, *args) :
        Gtk.main_quit()

    def __getitem__(self, name) :
        """ Return the value of Gtk object."""
        key = self.interface.get_object(name)
        if isinstance(key, Gtk.Entry) :
            return key.get_text()
        elif isinstance(key, Gtk.TextBuffer) :
            s, e = key.get_bounds()
            return key.get_text(s, e, False)
        elif isinstance(key, Gtk.FileChooserButton) :
            return key.get_filename()
        elif isinstance(key, Gtk.ComboBoxText) :
            return key.get_active()
        elif isinstance(key, Gtk.CheckButton) :
            return key.get_active()
        #elif isinstance(key, Gtk.Button) :
        #    key.get_visible()
        else :
            print(f"Unknow get fonction for type \"{type(key)}\" : {name}")

    def __setitem__(self, name, value) :
        """ Set the value of Gtk object."""
        key = self.interface.get_object(name)
        if isinstance(key, Gtk.Label) :
            key.set_label(value)
        elif isinstance(key, Gtk.FileChooserButton) :
            key.set_filename(value)
        elif isinstance(key, Gtk.ListStore) :
            for i in value : key.append([i,])
        elif isinstance(key, Gtk.ComboBoxText) :
            key.remove_all()
            for i in value : key.append_text(i)
            key.set_active(0)
        elif isinstance(key, Gtk.CheckButton) :
            return key.set_active(value)
        elif isinstance(key, Gtk.TextBuffer) :
            end_iter = key.get_end_iter()
            key.insert(end_iter, value)
            #return key.set_text(value)#, len(value))
        else :
            print(f"Unknow set fonction for type \"{type(key)}\"")

    def new_model_selected(self, *args) :
        """ Block check button for english only."""
        if self['model_size'] == 4 :
            self.interface.get_object("english_only").set_sensitive(0)
        else :
            self.interface.get_object("english_only").set_sensitive(1)

    def convert_s_to_hms(self, t) :
        """ Convert seconds in hours, minutes, seconds."""
        h = int(t // 3600)
        m = int((t % 3600) // 60)
        s = round(t % 60, 3)
        return h, m, s

    def __set_interface_occuped(self) :
        """ Disable widget sensitive, and switch start transcription button
        to stop transcription button."""
        self.__lock = False
        for i in self.widgets_name :
            k = self.interface.get_object(i).set_sensitive(False)
        self.interface.get_object("start_transcription").set_visible(False)
        self.interface.get_object("stop_transcription").set_visible(True)

    def __set_interface_allowed(self, change_label=True) :
        """ Re-enable widget sensitive, and switch stop transcription button
        to start transcription button."""
        for i in self.widgets_name :
            k = self.interface.get_object(i).set_sensitive(True)
        self.interface.get_object("start_transcription").set_visible(True)
        self.interface.get_object("stop_transcription").set_visible(False)
        if change_label :
            self['label_loading'] = "✗"
            self['language_loading'] = "✗"
            self['label_transcription'] = "✗"

    def transcribe(self) :
        """ Transcribe audio to text."""
        self.__set_interface_occuped()

        #get infos
        model_name = self.models[self['model_size']]
        if self['english_only'] : model_name += ".en"
        language = self.languages[self['language']]

        #load model
        self['label_loading'] = "..."
        options = {'compute_type' : self.precision[self['precision']]}
        if self['use_gpu'] :
            options['device'] = 'cuda'
        try :
            model = WhisperModel(model_name, **options)
        except ValueError:
            text = "Erreur : La precision choisie n'est pas disponible sur votre matériel.\n"
            text += f"<b>Veuillez choisir une autre option que \"{options['compute_type']}\".</b>"
            self.show_error("Precision not available", text)
            self.stop_transcription()
        except RuntimeError:
            text = f"Erreur : Vous n'avez pas de carte graphique dédiée, l'option doit donc être décochée."
            self.show_error("Pas de carte graphique", text)
            self.interface.get_object("use_gpu").set_active(False)
            self.interface.get_object("use_gpu").set_sensitive(False)
            self.widgets_name.remove("use_gpu")
            self.stop_transcription()

        if self.__lock : #break transcription and re-enabled widgets
            self.__set_interface_allowed()
            return False
        self['label_loading'] = "✔"

        #load audio file
        self['language_loading'] = "..."
        if language == "auto" : options = {}
        else : options = {'language' : language}
        segments, info = model.transcribe(self['input_file'], beam_size=5, **options)

        if self.__lock : #break transcription and re-enabled widgets
            self.__set_interface_allowed()
            return False
        self['language_loading'] = f"✔ ({info.language})"

        #transcribe
        self['label_transcription'] = "..."
        self.text = [] #{start:* , end:* , text:*}
        for segment in segments :
            print(f"[{round(segment.start, 2)} -> {round(segment.end, 2)}] : {segment.text}")
            self.text.append({
                "start" : round(segment.start, 3),
                "end"   : round(segment.end, 3),
                "text"  : segment.text
            })
            if len(self.text) == 0 :
                self['textview'] = segment.text.strip().capitalize()
            else :
                self['textview'] = segment.text
            #break, and no write current in files if user stop transcription
            if self.__lock :
                self.__set_interface_allowed()
                return False
        self['label_transcription'] = "✔"

        #save to files
        if self['save_to_file'] :
            out_file = f"{os.path.splitext(self['input_file'])[0]}.txt"
            with open(out_file, 'w') as file :
                for i in self.text :
                    file.write(i['text'])

        if self['generate_subtitle_file'] :
            out_file = f"{os.path.splitext(self['input_file'])[0]}.srt"
            with open(out_file, 'w') as file :
                for c, i in enumerate(self.text) :
                    start = self.convert_s_to_hms(i['start'])
                    start = ":".join([str(i) for i in start]).replace(".", ",")
                    end = self.convert_s_to_hms(i['end'])
                    end = ":".join([str(i) for i in end]).replace(".", ",")
                    file.write(f"{c}\n")
                    file.write(f"{start}0 --> {end}0\n")
                    file.write(f"{i['text']}\n\n")

        self.__set_interface_allowed(change_label=False)

    def stop_transcription(self, *args) :
        self.__lock = True

    def run_transcribe(self, *args) :
        """ Run transcription in a thread ;)"""
        #verify file exists
        if not os.path.exists(str(self['input_file'])) :
            self.show_error("File not found", "Erreur : fichier non trouvé")
            return False
        Thread(target=self.transcribe).start()

    def show_error(self, title, text) :
        os.system(f"zenity --error --width=300 --title=\"{title}\" --text=\"{text}\"")

if __name__ == "__main__" :
    app = FastWhisperGui()
    Gtk.main()
