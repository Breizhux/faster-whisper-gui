#!/bin/bash

initial_dir="$PWD"
work_dir="faster-whisper-gui"

error() {
    echo "An error is occured."
    echo "$1"
    exit 2
}

dir_to_deb() {
    #usage: dir_to_deb.sh "{source_dir}" "{destination.deb}"
    source_dir="$1"
    destination="$2"

    if [ -e "$destination" ]; then
        rm "$destination"
    fi
    chmod -R 755 "$source_dir/DEBIAN/"
    dpkg-deb --build "$source_dir"
    chmod 755 "$destination"
}

need_apt_dependency() {
    echo "To construct package, the script need \"virtualenv\" and \"python Gtk\" packages."
    echo "You can resolve with : sudo apt install -y virtualenv python3-gi"
    echo "Please, install it and rerun this script."
    exit 1
}

#prepare build dir
mkdir "$work_dir"
cp -R usr/ "$work_dir"
cp -R DEBIAN/ "$work_dir"
cd "$work_dir"

#create virtualenv
cd usr/share/faster-whisper-gui/
if [ ! -e bin/activate ]; then
    virtualenv ./ || need_apt_dependency
fi
source bin/activate

#install dependency
pip install -r requirements.txt

#apt install -y virtualenv python3-gi zenity
if [ ! -e /usr/lib/python3/dist-packages/gi ]; then
    need_apt_dependency
fi
ln -s /usr/lib/python3/dist-packages/gi ./lib/python3.9/site-packages/ || need_apt_dependency

#Create deb package
cd "$initial_dir"
dir_to_deb "$work_dir/" "$work_dir.deb" || error "Error when generate debian package."

#remove build dir
rm -R --force "$work_dir"

echo ""
echo "Debian package generated !"
echo ""
echo "Run this command to terminate installation :"
echo -e "\033[0;33msudo apt install ./$work_dir.deb\033[0m"
echo "After, you can remove this downloaded directory."
echo ""
